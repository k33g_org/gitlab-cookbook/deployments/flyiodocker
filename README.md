# fly.io-docker

## Refs

- https://fly.io/docs/hands-on/start/
- https://levelup.gitconnected.com/complete-guide-to-create-docker-container-for-your-golang-application-80f3fb59a15e
- https://dev.to/karanpratapsingh/dockerize-your-go-app-46pp
- https://github.com/karanpratapsingh/tutorials/tree/master/go/dockerize%20go

## Prerequisites

### Create an account on Fly.io (if it's your first time with Fly.io)

```bash
flyctl auth signup
# you need a credit card
```

### Create a Fly.io token

- Use https://web.fly.io/user/personal_access_tokens/new
- Copy the generated value
- If you work with Gitpod:
  - create an environment variable `FLY_ACCESS_TOKEN` (with the token as value) in your Gitpod account
- **🖐️ about GitLab CI**: create an environment variable `FLY_ACCESS_TOKEN` (with the token value) at the group level or project level 
  > - I used this level https://gitlab.com/groups/k33g_org/gitlab-cookbook/-/settings/ci_cd

## Deploy

- See 👀 `.gitlab-ci.yml`
